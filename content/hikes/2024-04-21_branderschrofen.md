---
title: "Tegelberg & Branderschrofen"
planned_date: 2024-04-28
facts:
  distance: 12.7
  duration: 6:15
  ascent: 1149
  descent: 1127
  highest: 1880
  lowest: 801
  difficulty: medium
  departure: 07:15
  return: 18:00
commute:
  - from: 'Munich Main Station'
    departure: 07:39
    by: RB 74
    to: Buchloe
    arrival: 08:22
meet:
  - location: Munich Main Station
    lng: 48.1414729
    lat: 11.5566653
    time: 07:15
route: https://www.alpenvereinaktiv.com/de/tour/tegerberg-branderschrofen/290765198/?share=%7Ezydyajdt%244ossvrfm
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather (the highes point is close to 2000m, it could be colder there)
  - Rain-Protection / Sun-Protection
  - Drinks and Snacks
  - Enough stamina for this hike
sitemap_exclude: true
draft: true
---
For all prices and princesses, we pass your dream castle, the [Neuschwanstein Castle](https://en.wikipedia.org/wiki/Neuschwanstein_Castle), but we are on a hike, so, see it, adjust your crown, take a picture, move on.

It's unfortunately further away than many other hikes, so we leave early. Once we arrive at Hohenschwangau, we can see the castle. On our [route][route], we pass the castle and have a short stop at the [Marienbrücke](https://www.hohenschwangau.de/natur-umgebung/marienbruecke-1), above the Pöllatschlucht to take another picture of the castle.  
We walk further along the [Maximilansweg](https://en.wikipedia.org/wiki/Maximiliansweg). Here we can decide, if we keep taking the Maximiliansweg or walk along the ridge of the Tegelbergkopf, which is harder that the Maximiliansweg, we can also split and meet again at the [Tegelberghaus](https://www.bergwelten.com/h/tegelberghaus), from where it's another kilometer distance and 180 meters up from here to the Branderschrofen. If the tour was to much, you can wait at the Tegelberghaus for us to return.  
We take another route down, passing the Rohrkopfhütte and take the bus back to the train at the cable station.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[route]: https://www.alpenvereinaktiv.com/de/tour/tegerberg-branderschrofen/290765198/?share=%7Ezydyajdt%244ossvrfm
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html