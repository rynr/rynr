---
title: "Brecherspitze (10km, 919hm)"
planned_date: 2024-05-19
facts:
  distance: 10.1
  duration: 5:00
  ascent: 919
  descent: 920
  highest: 1683
  lowest: 806
  difficulty: medium
  departure: 07:04
  return: 19:32
commute:
  - from: 'Munich Main Station'
    departure: 7:30
    by: BRB RB77
    to: Fischhausen-Neuhaus
    direction: Bayrischzell
    arrival: 8:38
  - from: Fischhausen-Neuhaus
    to: Brecherspitze
    direction: Bahnhof Tegernsee
    departure: 8:45
  - from: Brecherspitze
    departure: 13:00
    to: Ankl-Alm
  - from: Ankl-Alm
    to: Fischhausen-Neuhaus
    departure: 14:15
  - from: Fischhausen-Neuhaus
    to: Munich
    departure: 15:20, 16:20, 17:20
    by: BRB RB55
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:15
  - location: München Harras
    geo: 48°07'03.5"N 11°32'11.0"E
    time: 7:37
  - location: Holzkirchen
    geo: 47°53'04.0"N 11°41'49.2"E
    time: 7:56
  - location: Fischhausen-Neuhaus
    geo: 47°42'21.6"N 11°52'27.5"E
    time: 8:40
route: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-05-19/290870860/?share=%7Ezyekmaip%244ossvasu
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Sun-Protection
  - Drinks and Snacks
  - Some money for the Ankl-Alm
sitemap_exclude: true
---
The [Tour][route] is again close to the Schliersee. If you want, you can take swimming clothes with you and have a longer stay after the tour.  
We start in Neuhaus Train station. The standard tour goes along some paths which can be driven by car, but we already leave the road earlier. We go right, it directly goes up very steep to the first viewing point, the [Anklspitz](https://de.wikipedia.org/wiki/Ankelspitz). From there it's not far to the [Dürnbachwand](https://de.wikipedia.org/wiki/D%C3%BCrnbachwand). We now go along the ridge, which is not the standard path for anybody. There's at least one short climbing to be done without any belay.  
Once we reached the Brecherspitz Vorgipfel, there's another climbing along the ridge until we reach the [Brecherspitz](https://en.wikipedia.org/wiki/Brecherspitz) itself, our main goal.  
On the way down, we pass the Ankl-Alm, which hopefully serves us something to drink, before we go down along the street we skipped at the beginning.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[route]: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-05-19/290870860/?share=%7Ezyekmaip%244ossvasu
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html