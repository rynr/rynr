---
title: "Leonhardstein / Seekarkreuz"
planned_date: 2024-05-12
facts:
  distance: 20.2
  duration: 7:50
  ascent: 1298
  descent: 1394
  highest: 1601
  lowest: 679
  difficulty: medium
  departure: 07:04
  return: 19:32
commute:
  - from: 'Munich Main Station'
    departure: 7:04
    by: BRB RB57
    to: Gmund (Tegernsee)
    direction: Tegernsee
    arrival: 8:00
  - from: Gmund a. Tegernsee
    by: Bus 359
    to: Weißachbrücke, Rottach-Egern
    direction: Bahnhof Tegernsee
    departure: 8:04
    arrival: 8:29
  - from: Weißachbrücke, Rottach-Egern
    by: Bus 356
    departure: 8:32
    direction: Wildbad, Kreut
    to: Riedlerbrücke, Kreuth
    arrival: 8:48
  - from: Riedlerbrücke
    to: Leonhardstein
    departure: 8:50
  - from: Leonhardstein
    to: Seekarkreuz
    departure: 11:30
  - from: Seekarkreuz
    to: Lenggrieser Hütte
    departure: 15:00
  - from: Lenggrieser Hütte
    to: Lenggres
    departure: 16:00
  - from: Lenggries
    to: Munich
    departure: 18:17, 18:47, 19:17
    by: BRB RB55
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 6:50
  - location: München Harras
    geo: 48°07'03.5"N 11°32'11.0"E
    time: 7:11
  - location: Holzkirchen
    geo: 47°53'04.0"N 11°41'49.2"E
    time: 7:30
  - location: Gmund, Tegernsee
    geo: 47°45'00.4"N 11°44'09.1"E
    time: 8:00
  - location: Riedlerbrücke, Kreuth
    geo: 47°38'39.6"N 11°44'42.4"E
    time: 8:48
route: https://www.alpenvereinaktiv.com/de/tour/leonhardstein-seekar/290995298/?share=%7Ezyfsnswn%244ossvini
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather, we will probably have snow even on the path somewhere.
  - Sun-Protection
  - Drinks and Snacks
  - Some money for the Lenggrieser Hütte
  - Enough stamina for this hike
sitemap_exclude: true
---
The [Tour][route] is this time between Tegernsee and Lenggries.  
We start in [Kreuth](https://www.tegernsee.com/kreuth) with the first destination of the [Leonhardstein](https://en.wikipedia.org/wiki/Leonhardstein) (yes, wikipedia has almost nothing about that summit, also the [german version](https://de.wikipedia.org/wiki/Leonhardstein) doesn't have much more to say), from where we have a nice view on the Tegernsee.  
Getting up (and down again) the Leonhardstein will be the most steep part. This tour is more distance than height meters, so we head to the [Seekarkreuz](https://en.wikipedia.org/wiki/Seekarkreuz) (where wikipedia has even less to say about). The thing you need to know is, there's the [Lenggrieser Hütte](http://www.lenggrieserhuette.de/) nearby, where we can stop for a drink or something to eat.

**Please be aware**: This hike is 20km, there's no exit halfway.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[route]: https://www.alpenvereinaktiv.com/de/tour/leonhardstein-seekar/290995298/?share=%7Ezyfsnswn%244ossvini
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html