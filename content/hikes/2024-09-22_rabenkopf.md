---
title: "Rabenkopf 🔴 (medium, 1102hm, 7:00h)"
planned_date: 2024-07-20
facts:
  distance: 15.1 km
  duration: 6:20 h
  ascent: 1135 hm
  descent: 1135 hm
  highest: 1554 m
  lowest: 606 m
  difficulty: medium
  departure: 07:59
  return: 19:32
commute:
  - from: 'München'
    departure: 7:59
    by: RB 66
    to: Kochel
    arrival: 9:09
  - from: Kochel
    departure: 9:10
    to: Sonnenspitz
  - from: Sonnenspitz
    to: Rabenkopf
    departure: 11:30 
  - from: Rabenkopf
    to: Kochel
    departure: 14:00
  - from: Kochel
    to: München
    departure: 15:44, 16:44, 17:44
    arrival: 16:59, 18:00 19:00
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:45
  - location: 'Kochel'
    geo: 47°39'37.3"N 11°22'14.6"E
    time: 09:10
route: https://www.alpenvereinaktiv.com/de/tour/rabenkopf/302326645/?share=%7Ezzpxtmwm%244ossgotj
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Sun-Protection
  - Drinks and Snacks
  - Warm clothes (it might get colder by altitude)
sitemap_exclude: true
---
No [Wiesn](https://www.oktoberfest.de/) this weekend? We got you covered.  
If you went to the Wiesn the day before, we even start later than usual.

I did the [tour][route] before, but not the way back, so we'll see how it goes.  
Starting in Kochel, we walk through the town until we reach the woods. We either walk along a wider road, I planned taking smaller paths, first to [Sonnenspitz](https://de.wikipedia.org/wiki/Sonnenspitz_(Kochel)). From here through a wide valley into a steeper valley up to the Rabenspitze.  
As I wrote, I haven't yet taken the path down, we might also choose another route, but that's the fastest way back.

There's no hut on the way this time, so have enough to eat and drink with you.

[route]: https://www.alpenvereinaktiv.com/de/tour/rabenkopf/302326645/?share=%7Ezzpxtmwm%244ossgotj