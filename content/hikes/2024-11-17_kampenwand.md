---
title: "Kampenwand 🔴 (medium, 1047hm, 6h)"
planned_date: 2024-11-17
facts:
  distance: 15 km
  duration: 6 h
  ascent: 1047
  descent: 1047
  highest: 1652
  lowest: 605
  difficulty: medium
  departure: 07:56
  return: 18:00
commute:
  - from: 'München'
    departure: 7:56
    by: BRB RE5
    to: Prien am Chiemsee
    direction: Salzburg
    arrival: 8:51
  - from: Prien am Chiemsee
    departure: 9:16
    by: RB 92
    to: Aschau(Chiemgau)
    direction: Aschau(Chiemgau)
    arrival: 9:30
  - to: Kampenwand
    from: Aschau(Chiemgau)
    departure: 9:30
  - from: Kampenwand
    to: Steinlingalm
  - from: Steinlingalm
    to: Aschau(Ciemgau)
  - from: Aschau(Chiemgau)
    to: Prien am Chiemsee
    departure: 15:43, 16:25, 17:17(Bus 496 über Rosenheim), 18:46
    arrival: 15:57, 16:39, 19:00
  - from: Prien am Chiemsee
    to: München
    departure: 16:06, 17:06. 19:06
    arrival: 17:06, 18:06, 20:06
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:45
  - location: Munich Ost
    geo: 48°07'38.3"N 11°36'21.4"E
    time: 7:45
  - location: Aschau(Chiemgau)
    geo: 47°46'44.7"N 12°19'30.3"E
    time: 9:15
route: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-11-15/305667955/?share=%7E3ofuylne%244ossutty
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Drinks and Snacks
  - Warm clothes (forecast says 4°C)
sitemap_exclude: true
---
The [Kampenwand](https://en.wikipedia.org/wiki/Kampenwand) has the biggest cross in the bavarian alps.  
We once have to change the train and start from Aschau. There's not much to tell about the [route][route]. Before the last ascent to the Kampenwand, we pass the [Steinlingalm](https://www.steinlingalm.de/). If you do not want to take the final ascent, you can have a break here and wait for us to return.  
There's some part right before the summit where it's steep with metal-wires for safety (checkout some [video](https://www.youtube.com/watch?v=aW8zcMAmCoM) around 8:20), so you can stay at the alm if that's not for you.  
There will be clouds on the afternoon, we'll see if we can still have some sun. It will also be windy, so decide wise, if you want to make the summit.

I will join at München Ost. If you want to meet at München Hbf, check who else is there.

I changed the registration to close on Saturday 7pm, if you cancel after, you steal somebodies chance to join.

As people always ask for some WhatsApp Group, I now experiment with it, there's a [Kampenwand](https://chat.whatsapp.com/Baq3ySV6OJ0Gkgolm9DmYE) group just for this hike.

[route]: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-11-15/305667955/?share=%7E3ofuylne%244ossutty