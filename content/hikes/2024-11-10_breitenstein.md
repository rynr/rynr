---
title: "Breitenstein 🔵 (easy, 778hm, 3:50h)"
planned_date: 2024-11-10
facts:
  distance: 8 km
  duration: 3:50 h
  ascent: 778
  descent: 778
  highest: 1621
  lowest: 851
  difficulty: easy
  departure: 07:30
  return: 18:00
commute:
  - from: 'München'
    departure: 7:30
    by: BRB RB 77355
    to: Miesbach
    direction: Bayrischzell
    arrival: 8:13
  - from: Miesbach
    departure: 8:14
    by: Bus 352
    to: Birkenstein, Fischbachau
    direction: Fischhausen Neuhaus, Schliersee
    arrival: 9:11
  - to: Breitenstein Westgipfel
    from: Birkenstein, Fischbachau
    departure: 9:15
  - from: Breitenstein Westgipfel
    to: Breitenstein
  - from: Breitenstein
    to: Hubertushütte
  - from: Hubertushütte
    to: Kesselalm
  - from: Kesselalm
    to: Birkeinstein or Fischbachau
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:15
  - location: Birkenstein, Fischbachau
    geo: 47°42'47.7"N 11°57'49.6"E
    time: 9:15
route: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-11-07/305304090/?share=%7E3oace9mg%244osstzek
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Drinks and Snacks
  - Warm clothes (it is getting colder)
sitemap_exclude: true
---
The [tour][route] starts at [Birkenstein](https://de.wikipedia.org/wiki/Birkenstein_(Fischbachau)). There's a [Wallfahrtskapelle](https://www.maria-birkenstein.de/) close to the start.  
The tour is relatively simple, go up, then go down. We head north for the ascend, at the beginning through some wood, once exitig the woods there are some stones and it gets steeper, but it's not that far.
On the more steep descend we pass two huts. The [Hubertushütte](https://www.fischbachau.de/a-bergwirtschaft-hubertushuette) might still be open, but we can definitively have a stop at the [Kesselalm](https://kesselalm.com/), as the are also open during the winter.

I didn't plan the return by public transport, the bus doesn't go that often. We might decide to walk to Fischbachau. But I intend to be back before the sun goes down.

I changed the registration to close on Saturday 2pm, please cancel before if you figure out you'll not going to make it, so you can be sure you're in Saturday afternoon.

As people always ask for some WhatsApp Group, I now experiment with it, there's a [Breitenstein](https://chat.whatsapp.com/FLfgWgouQL27R9Dhoukh7I) group just for this hike.

[route]: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-11-07/305304090/?share=%7E3oace9mg%244osstzek