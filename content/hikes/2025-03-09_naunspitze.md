---
title: "Naunspitze 🔴 (medium, 1153hm, 7:30h)"
planned_date: 2025-03-09
facts:
  distance: 20 km
  duration: 7:30 h
  ascent: 1157
  descent: 1157
  highest: 1633
  lowest: 482
  difficulty: medium
  departure: 07:29
  return: 18:00
commute:
  - from: 'München Hbf'
    departure: 7:38
    by: BRB RB54
    to: Kufstein
    arrival: 8:58
  - from: Kufstein
    to: Ritzaualm
  - from: Ritzaualm
    to: Naunspitze
  - from: Naunspitze
    to: Kufstein
  - from: Kufstein
    to: München
    by: BRB RB54
    departure: 16:02, 17:02, 18:02, 19:01
    arrival: 17:18, 18:18, 19:18, 20:19
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:20
  - location: München Ost
    geo: 48°07'38.1"N 11°36'19.1"E
    time: 7:50
  - location: Grafing
    time: 8:03
  - location: Rosenheim
    time: 8:24
  - location: Kufstein
    geo: 47°35'02.3"N 12°09'56.9"E
    time: 9:00
route: https://www.alpenvereinaktiv.com/de/tour/aussichtsreiche-rundwanderung-auf-dem-nordalpenweg-01-von-kufstein/310851997/?share=%7E3i7kofia%244ossgcco
bring:
  - Worn-in hiking-boots (should be waterproof)
  - Clothing according to the weather
  - Warm clothes
  - Drinks (maybe something warm) and Snacks
  - Crampons (optional)
sitemap_exclude: true
---
The weather forecast looks fine.

As the last hike, the [tour][route] starts in [Kufstein](https://www.kufstein.com/). Going east, we'll be a short time in the shadow of the mountains of the [Wilder Kaiser](https://www.wilderkaiser.info/), but we soon are on the side heading to the sun.  
Unfortunately, the [Vorderkaiserfeldenhütte](https://www.alpenverein-muenchen-oberland.de/huetten/alpenvereinshuetten/vorderkaiserfeldenhuette/baumassnahmen-vorderkaiserfeldenhuette) has closed for renovation, but we can have a stop at the [Ritzaualm](https://www.ritzaualm.com/).  
If you're already done, you can stay here while everybody else heads to the [Naunspitze](https://en.wikipedia.org/wiki/Naunspitze).

I don't assume too much snow, but better bring some [crampons][crampons].  

I will enter at [München Ost](https://www.bahnhof.de/muenchen-ost), if you join at [München Hbf](https://www.bahnhof.de/muenchen-hbf), please reserve some seats. To align you can join a [WhatsApp Group](https://chat.whatsapp.com/B9xva5nMlMp9VfsIIKGT1q).

[route]: https://www.alpenvereinaktiv.com/de/tour/aussichtsreiche-rundwanderung-auf-dem-nordalpenweg-01-von-kufstein/310851997/?share=%7E3i7kofia%244ossgcco
[crampons]: https://en.wikipedia.org/wiki/Crampons