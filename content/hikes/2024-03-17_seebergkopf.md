---
title: "First Hike of the Year - Seebergkopf"
sitemap_exclude: true
planned_date: 2024-03-17
facts:
  distance: 13.8
  duration: 5
  ascent: 814
  descent: 814
  highest: 1538
  lowest: 778
  difficulty: easy
  departure: 07:04
  return: 18:00
commute:
  - from: Munich Main Station
    departure: 06:30
    by: BRB RB55
    direction: Bayrischzell
    to: Bayrischzell
    arrival: 07:57
  - from: Obergries
    departure: 08:10
    to: Oberer Wanderparkplatz Längenthal
    arrival: 09:00
meet:
  - location: Munich Main Station (Platforms 27-36)
    geo: 48°08'28.9"N 11°33'26.2"E
    time: 06:50
  - location: Obergries Station
    geo: 47°42'24.5"N 11°33'49.5"E
    time: 08:10
  - location: Vorderer Wanderparkplatz Längenthal
    geo: 47°42'05.1"N 11°31'37.6"E
    time: 09:00
bring:
  - Snowshoes & Poles (required)
  - Worn-in Hiking Boots
  - Warm clothing, Beanie, warm gloves
  - Drinks and Snacks (there's a hut, but you should have some on the tour)
  - Enough stamina for a long snowshoe hike
route: https://www.bergtour-online.de/bergtouren/bergwanderungen/mittel/seebergkopf-bayrischzell/
disclamer:  This counts especially in winter when there's more risk.
---
Happy new year, let's go for a snowshoe hike.  
I found this nice tour on the [DAV Tourentipps][route].

The start is not directly to reach without a car. It's another 3½km (45 minutes) by foot from and to the train station.  
If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html
[route]: https://www.alpenverein-muenchen-oberland.de/mitgliederservice/alpine-beratung-dav/tourentipps#16723277
