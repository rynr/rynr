---
title: "Kramerspitz (hard, 1354hm, 7:30h)"
planned_date: 2024-06-16
facts:
  distance: 17 km
  duration: 7:30 h
  ascent: 1354 hm
  descent: 1322 hm
  highest: 1982 m
  lowest: 691 m
  difficulty: hard
  departure: 06:32
  return: 19:32
commute:
  - from: 'Munich Main Station'
    departure: 7:30
    by: RE 61
    to: Garmisch-Patenkirchen
    direction: Mittenwald
    arrival: 08:23
  - from: Garmisch-Patenkirchen
    to: Felsen Kanzel
    departure: 08:25
  - from: Felsen Kanzel
    departure: 11:15
  - from: Mittergern
    to: Kramerspitz
    departure: 13:00
    arrival: 14:00
  - from: Kramerspitz
    to: Stepbergalm
    arrival: 15:30
  - from: Stepbergalm
    to: Untergrainau
  - from: Untergrainau
    to: Garmisch Patenkirchen Bahnhof
    by: RB60
    departure: 18:48, 19:48
  - from: Garmisch Patenkirchen Bahnhof
    to: Munich Main Station
    departure: 19:05, 20:05
    arrival: 20:26, 21:26
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 07:00
  - location: Garmisch Patenkirchen Station (Parking at the back)
    geo: 47°29'28.1"N 11°05'45.9"E
    time: 08:25
route: https://www.alpenvereinaktiv.com/de/tour/kramerspitz/294003306/?share=%7Ezyydoyiz%244ossqndi
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Sun-Protection
  - Drinks and Snacks
  - Money for the hut (optional)
sitemap_exclude: true
---
The [Kramerspitz](https://en.wikipedia.org/wiki/Kramerspitz) is a nice tour starting in Garmisch-Patenkirchen. We are close to 2000 height meters, but shortly below.  
We start in Garmisch-Patenkirchen and mainly go up, up, up. We shortly pass the [Felsen Kanzel](https://www.alpenvereinaktiv.com/de/punkt/felsenkanzel/16234139/), a viewing point. It further goes up.  
Passing the [Mittergern](https://www.openstreetmap.org/node/715851486), we have reached the first summit, which means we need to go down again. We walk along the back of the Kramer to eventually find the top.

To descend, we head west. After 2 kilometers we find the [Stepbergalm](http://www.stepberg-alm.de/). After a break here, we descent through a valley with steep sides. We don't return to Garmisch but take the next train leaving from Untergrainau.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[route]: https://www.alpenvereinaktiv.com/de/tour/kramerspitz/294003306/?share=%7Ezyydoyiz%244ossqndi
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html