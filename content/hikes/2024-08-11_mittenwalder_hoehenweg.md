---
title: "Mittenwalder Höhenweg 🔴"
planned_date: 2024-08-11
facts:
  distance: 12.1 km
  duration: 6:00 h
  ascent: 460 hm
  descent: 1800 hm
  highest: 2336 m
  lowest: 913 m
  difficulty: medium
  departure: 07:13
  return: 18:05
commute:
  - from: 'Munich Main Station'
    departure: 7:13
    by: RE 61
    to: Mittenwald Bahnhof
    arrival: 08:54
  - from: Mittenwald Bahnhof
    to: Karwendelbahn Talstation
    departure: 09:00
    arrival: 09:15
  - from: Karwendelbahn Talstation
    to: Karwendelbahn Bergstation
  - from: Karwendelbahn Bergstation
    to: Nördliche Linderspitze
  - from: Nördliche Linderspitze
    to: Brunnsteinanger
  - from: Brunnsteinanger
    to: Brunnsteinhütte
  - from: Brunnsteinhütte
    to: Mittenwald Bahnhof
  - from: Mittenwald Bahnhof
    to: München
    departure: 17:04, 18:04, 19:04
    arrival: 18:48, 19:48, 20:49
    by: RE 61
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 07:00
  - location: Mittenwald Bahnhof
    geo: 47°26'25.7"N 11°15'55.4"E
    time: 09:00
  - location: Karwendelbahn Talstation
    geo: 47°26'17.1"N 11°16'12.8"E
    time: 09:15
  - location: Karwendelbahn Bergstation
    geo: 47°25'48.2"N 11°17'45.5"E
    time: 09:45
route: https://www.alpenvereinaktiv.com/de/tour/mittenwalder-hoehenweg/298677671/?share=%7Ezzutn3ea%244ossoocg
bring:
  - Worn-in hiking-boots
  - Via-Ferrata-Set, Harness, Helmet, Glove
  - Clothing according to the weather
  - Sun-Protection
  - Drinks and Snacks
  - Money for the hut (optional)
  - Money for the Karwendelbahn
sitemap_exclude: true
---
Let's see how many would join a via ferrata.  
The [Mittenwalder Höhenweg][route] is a simple via ferrata close to [Mittenwald][mittenwald]. The hardest parts are [classified][topo] as `B` (see [grading][grading]):
> Some steep terrain, smaller footholds, but climbing aids provided. Longer ladders possible. Essentially protected but exposed scrambling. Some use of arms. .

We will take the Karwendelbahn to get to the beginning, the single trip does cost 25,90€.

There's usually an early exit, if you figure out you're not fine for the whole via ferrata, but the exit is currently closed. So you're stuck to the full via ferrata.  
At the end, we descend to the [Brunnsteinhütte][brunnsteinhuette], where we can have something to drink and eat, before we head back to the train station to return back to munich.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

You can join us any time later, you can also decide to ascent without the Karwendelbahn, just be on top on time and maybe tell me beforehand, so we will meet.

[brunnsteinhuette]: https://www.brunnsteinhuette.de/
[grading]: https://en.wikipedia.org/wiki/Via_ferrata#Grading
[topo]: https://www.bergsteigen.com/touren/klettersteig/mittenwalder-hoehenweg/
[mittenwald]: https://en.wikipedia.org/wiki/Mittenwald
[route]: https://www.alpenvereinaktiv.com/de/tour/mittenwalder-hoehenweg/298677671/?share=%7Ezzutn3ea%244ossoocg
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html