---
title: "Drachenkopf 🔴 (medium, 1462hm, 8:30h)"
planned_date: 2024-08-04
facts:
  distance: 20 km
  duration: 8:30 h
  ascent: 1462 hm
  descent: 1462 hm
  highest: 2296 m
  lowest: 974 m
  difficulty: medium
  departure: 07:13
  return: 19:05
commute:
  - from: 'Munich Main Station'
    departure: 7:13
    by: RE 61
    to: Garmisch-Patenkirchen
    direction: Mittenwald
    arrival: 08:23
  - from: Garmisch-Patenkirchen
    to: Ehrwald Zugspitzbahn
    by: RE 62
    direction: Lermoos
    departure: 08:26
    arrival: 08:54
  - from: Ehrwald Zugspitzbahn
    to: Seebensee
    departure: 09:00
  - from: Seebensee
    to: Coburger Hütte
  - from: Coburger Hütte
    to: Drachenkopf
  - from: Drachenkopf
    to: Ehrwald Zugspitzbahn
  - from: Ehrwald Zugspitzbahn
    to: Garmisch-Patenkirchen
    departure: 17:31, 18:31, 19:31
    arrival: 17:57, 18:47, 19:57
    by: RB 60
  - from: Garmisch-Patenkirchen
    to: Munich Main Station
    by: RB 6
    departure: 18:05, 19:05, 20:05
    arrival: 19:26, 20:26, 21:26
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 07:00
  - location: Garmisch Patenkirchen
    geo: 47°29'28.9"N 11°05'49.7"E
    time: 08:23
  - location: Ehrwald Zugspitzbahn
    geo: 47°24'34.2"N 10°54'49.4"E
    time: 09:00
route: https://www.alpenvereinaktiv.com/de/tour/drachensee/290485788/?share=%7Ezybhauwp%244ossuzbg
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Sun-Protection
  - Drinks and Snacks
  - Money for the hut (optional)
sitemap_exclude: true
---
The first tour this year above 2000hm (I had planned another one, but I like the Seebensee, Drachensee and the ascent).

The [route][route] starts in [Ehrwald][ehrwald], which is in Austria, so have your passport with you (as we arrive by german train which later returns to germany, the Deutschland Ticket is valid for the ride).  
Unfortunately we need to walk through the town to get to the ascent. The ascent is quite steep, there are also narrow passages, where you should not have any fear of heights. The path is classified as via ferrata (Klettersteig) with the name [Hoher Gang](https://klettersteig.de/klettersteig/hoher_gang/96), but it can be done without any gear, it's classified as difficulty `A/B` (checkou this [video](https://www.youtube.com/watch?v=H466Y9d5X20) to get an idea).

We arrive at the [Seebensee](https://en.wikipedia.org/wiki/Seebensee), which has a great view with the mountains behind them. Still some height meters further up, there's the [Coburger Hütte](https://www.coburgerhuette.at/). Close to it is the Drachensee.  
We will make a break here, so you can decide if you want to drink and eat at the hut, of if you want to go for a swim in the (cold) Drachensee.

We shoudln't stay too long as the walking time alone is 8½ hours, so we head to the Drachenkopf, which means some climbing before the summit.  
If you're exhausted, you can wait in the hut until we return.

The descent and path to the train station also takes quite a while, so we need to see, which train we can catch to return to munich.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[ehrwald]: https://www.ehrwald.gv.at/
[route]: https://www.alpenvereinaktiv.com/de/tour/drachensee/290485788/?share=%7Ezybhauwp%244ossuzbg
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html