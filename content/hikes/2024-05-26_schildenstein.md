---
title: "Schildenstein (hard, 900hm, 6:50h)"
planned_date: 2024-05-26
facts:
  distance: 14.5 km
  duration: 5:45 h
  ascent: 981 hm
  descent: 981 hm
  highest: 1616 m
  lowest: 800 m
  difficulty: hard
  departure: 06:32
  return: 19:32
commute:
  - from: 'Munich Main Station'
    departure: 7:30
    by: BRB BR57
    to: Tegernsee
    direction: Tegernsee
    arrival: 08:39
  - from: Tegernsee
    to: Abzw. Siebenhütten, Kreuth
    by: Bus 390
    direction: Pertisau
    departure: 08:45
    arrival: 09:19
  - from: Siebenhütten
    departure: 09:20
    to: Schildenstein
  - from: Schildenstein
    to: Platteneck
    departure: 13:00
  - from: Platteneck
    to: Siebenhütten
    departure: 14:00
  - from: Siebenhütten
    to: Tegernsee
    departure: 15:58, 16:58, 18:15
    by: Bus 356, Bus 390
  - from: Tegernsee
    to: Munich Main Station
    departure: 16:52, 17:52, 18:53
    by: BRb RB57
    arrival: 17:57, 18:57, 19:55
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 07:15
  - location: München Harras
    geo: 48°07'07.9"N 11°32'11.9"E
    time: 07:37
  - location: Holzkirchen
    geo: 47°53'04.6"N 11°41'48.5"E
    time: 07:56
  - location: Tegernsee
    geo: 47°42'50.3"N 11°45'27.0"E
    time: 08:39
  - location: Parkplatz Siebenhütten
    geo: 47°37'22.3"N 11°44'05.1"E
    time: 09:20
route: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-05-23/293094317/?share=%7Ezysrqylm%244ossmwvk
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Sun-Protection
  - Drinks and Snacks
sitemap_exclude: true
---
I defined this tour hard, because the Wolfsschlucht is not just a simple path to walk.  
We start easy reaching the Wolfsschlucht. The path is not always clear, we might need to cross a small steam. At the end of the Wolfsschlucht, it's going up. You'll need to use hands and feet, it's easy climbing. Here's a [video](https://www.youtube.com/watch?v=E0apGZHoXaM) to have an idea. There also smaller parts with some rope.  
Once on top it's not far away to reach the Schildenstein.

We don't take the direct way down, which most visitor do, we have a short stop at the next summit, the Platteneck. We even leave the path here to descent via the Königsalm back to where we started.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[route]: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-05-23/293094317/?share=%7Ezysrqylm%244ossmwvk
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html