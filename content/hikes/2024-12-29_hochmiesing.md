---
title: "Hochmiesing 🔴 (medium, 1116hm, 7h)"
planned_date: 2024-12-29
facts:
  distance: 19.7 km
  duration: 7 h
  ascent: 1116
  descent: 1116
  highest: 1883
  lowest: 771
  difficulty: medium
  departure: 07:38
  return: 18:20
commute:
  - from: 'München Hbf'
    departure: 7:29
    by: BRB RB55
    to: Geitau
    direction: Bayrischzell
    arrival: 8:49
  - from: Geitau
    to: Soinsee
    departure: 8:50
  - from: Soinsee
    to: Hochmiesing
  - from: Hochmiesing
    to: Geitau
  - from: Geitau
    direction: München
    by: BRB RB55
    to: München Hbf
    departure: 17:38, 18:38, 19:38
    arrival: 18:57, 19:57, 20:55
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:15
  - location: Geitau
    geo: 47°41'37.4"N 11°57'58.3"E
    time: 8:50
route: https://www.alpenvereinaktiv.com/de/tour/rundwanderung-auf-wanderweg-644-spitzingsee-von-geitau/307140829/?share=%7E3odshicx%244ossgumu
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Warm clothes (forecast says 4°C)
  - Drinks (maybe something warm) and Snacks
  - Crampons
  - Zipfelbob os something similar (optional)
sitemap_exclude: true
---
The weather this sunday looks very very good, so let's have one last hike this year, and also the last hike I organize on this meetup group before it will be closed in march.

The [tour][route] is not particularly hard, but there's snow, last time I was there, there was also ice, so bringing some [Crampons][crampons] is a good idea.

It can be quite windy at the top, so best have some warm drinks with you as well. There's no hut this time to take a break and get something to eat and drink.

Getting down can also be more fun if you have some simple [sled][zipfelbob] with you.

**RSVP closes on Saturday 6pm**, please update your status before, so everybody has the chance to join.

[route]: https://www.alpenvereinaktiv.com/de/tour/rundwanderung-auf-wanderweg-644-spitzingsee-von-geitau/307140829/?share=%7E3odshicx%244ossgumu
[crampons]: https://en.wikipedia.org/wiki/Crampons
[zipfelbob]: https://de.wikipedia.org/wiki/Zipflbob