---
title: "Gamskogel 🔵 (easy, 1116hm, 7h)"
planned_date: 2025-02-16
facts:
  distance: 19.3 km
  duration: 7 h
  ascent: 969
  descent: 969
  highest: 1448
  lowest: 481
  difficulty: easy
  departure: 07:29
  return: 18:00
commute:
  - from: 'München Hbf'
    departure: 7:38
    by: BRB RB54
    to: Kufstein
    arrival: 8:58
  - from: Kufstein
    to: Gamskogel
  - from: Gamskogel
    to: Berghaus Aschenbrenner
  - from: Berghaus Aschenbrenner
    to: Kufstein
  - from: Kufstein
    to: München
    by: BRB RB54
    departure: 16:02, 17:02, 18:02, 19:01
    arrival: 17:18, 18:18, 19:18, 20:19
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:20
  - location: Grafing
    time: 8:03
  - location: Rosenheim
    time: 8:24
  - location: Kufstein
    geo: 47°35'02.3"N 12°09'56.9"E
    time: 9:00
route: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2025-02-13/309618689/?share=%7E3oyqcnfi%244ossqoec
bring:
  - Worn-in hiking-boots (should be waterproof)
  - Clothing according to the weather
  - Warm clothes
  - Drinks (maybe something warm) and Snacks
  - Crampons (optional)
  - Zipfelbob os something similar (optional)
sitemap_exclude: true
---
It's going to be cold, but we should see the sun.

The [tour][route] is not as it could be. We start te hike from Kufstein, pass the Weinbergerhaus, but they are currently closed. As well as some other paths, so we have a longer passage we go in both directions.  
When heading down, we pass the [Berghaus Aschenbrenner](https://www.berghaus-aschenbrenner.com/), where we can have something to drink or even eat. Then we return to Kufstein, and back home.

I don't assume too much snow, but better bring some [crampons][crampons].  
For the descent, it might make sense to bring a [Zipfelbob][zipfelbob] or something else to slide back to Kufstein.

If you don't make the whole 19.3km, you can leave earlier for the Berghaus, we meet there later.

[route]: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2025-02-13/309618689/?share=%7E3oyqcnfi%244ossqoec
[crampons]: https://en.wikipedia.org/wiki/Crampons
[zipfelbob]: https://de.wikipedia.org/wiki/Zipflbob