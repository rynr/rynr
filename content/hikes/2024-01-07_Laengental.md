---
title: "Snowshoe-Hike - Längental, Längenberg-Alm (1.244m) and Kirchsteinhütte"
sitemap_exclude: true
planned_date: 2024-01-07
facts:
  distance: 15.6
  duration: 5
  ascent: 674
  descent: 674
  highest: 1242
  lowest: 724
  difficulty: medium
  departure: 07:04
  return: 18:00
commute:
  - from: Munich Main Station
    departure: 07:04
    by: BRB RB56
    direction: Lenggries
    to: Obergries Train Station
    arrival: 08:08
  - from: Obergries
    departure: 08:10
    to: Oberer Wanderparkplatz Längenthal
    arrival: 09:00
meet:
  - location: Munich Main Station (Platforms 27-36)
    geo: 48°08'28.9"N 11°33'26.2"E
    time: 06:50
  - location: Obergries Station
    geo: 47°42'24.5"N 11°33'49.5"E
    time: 08:10
  - location: Vorderer Wanderparkplatz Längenthal
    geo: 47°42'05.1"N 11°31'37.6"E
    time: 09:00
bring:
  - Snowshoes & Poles (required)
  - Worn-in Hiking Boots
  - Warm clothing, Beanie, warm gloves
  - Drinks and Snacks (there's a hut, but you should have some on the tour)
  - Enough stamina for a long snowshoe hike
route: https://www.alpenverein-muenchen-oberland.de/mitgliederservice/alpine-beratung-dav/tourentipps#16723277
disclamer:  This counts especially in winter when there's more risk.
draft: true
---
Happy new year, let's go for a snowshoe hike.  
I found this nice tour on the [DAV Tourentipps][route].

The start is not directly to reach without a car. It's another 3½km (45 minutes) by foot from and to the train station.  
If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html
[route]: https://www.alpenverein-muenchen-oberland.de/mitgliederservice/alpine-beratung-dav/tourentipps#16723277
