---
title: "Benediktenwand"
planned_date: 2024-04-28
facts:
  distance: 12.9
  duration: 6:15
  ascent: 1277
  descent: 1277
  highest: 1939
  lowest: 670
  difficulty: medium
  departure: 07:15
  return: 18:00
commute:
  - from: 'Munich Main Station'
    departure: 07:32
    by: RB 6
    direction: Garmisch-Partenkirchen
    to: Farchant
    arrival: 08:52
  - from: Farchant
    departure: 8:52 
    to: Hoher Fricken
  - from: Hoher Fricken
    departure: 12:30
    to: Farchant
  - from: Farchant
    to: München Hbf
    by: RB 6
    departure: 15:11, 16:11, 17:09
meet:
  - location: Munich Main Station
    lng: 48.1414729
    lat: 11.5566653
    time: 07:15
  - location: Farchant Station
    lng: 47.5305322
    lat: 11.1119468
    time: 08:52
route: https://www.alpenvereinaktiv.com/de/tour/hoher-fricken/290143842/?share=%7Ezyydobl4%244ossubpm
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather (the highes point is close to 2000m, it could be colder there)
  - Rain-Protection / Sun-Protection
  - Drinks and Snacks
  - Enough stamina for this hike
sitemap_exclude: true
draft: true
---
Let's explore another area of the alps close to munich.  
Close to [Garmisch-Patenkirchen](https://en.wikipedia.org/wiki/Garmisch-Partenkirchen), there's the [Hoher Fricken](https://de.wikipedia.org/wiki/Hoher_Fricken).
Our [route](https://www.alpenvereinaktiv.com/de/tour/hoher-fricken/290143842/?share=%7Ezyydobl4%244ossubpm) is nothing special, but with a height of 1939m we are close to 2000. We start from [Farchant](https://maps.app.goo.gl/trAnbLRsuZ6kyCDaA), which is one station before Garmisch-Patenkirchen.

This time, there's no hut on the tour (there's the [Almgasthof Esterberg](https://maps.app.goo.gl/bzus6ZLVaYmDsrcd6), but it's not really on the path, we might decide to take a detour if we're back early).

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html