---
title: "Notkarspitze 🔴 (medium, 1091hm, 6h)"
planned_date: 2024-12-01
facts:
  distance: 11 km
  duration: 6 h
  ascent: 1091
  descent: 1095
  highest: 1888
  lowest: 839
  difficulty: medium
  departure: 07:56
  return: 18:00
commute:
  - from: 'München'
    departure: 7:32
    by: RB 6
    to: Oberau
    direction: Garmisch Patenkirchen
    arrival: 8:48
  - from: Oberau
    to: Klostergasthof Ettal
    by: Bus 9606
    direction: Oberammergau
    departure: 08:55
    arrival: 09:05
  - from: Ettal
    to: Notkarspitze
    departure: 09:10
  - from: Notkarspitze
    to: Ettal
  - from: Am Berg, Ettal
    direction: Garmisch-Patenkirchen
    by: Bus 9606
    to: Bahnhof Oberau
    departure: 15:57, 16:57, 17:57, 18:57
    arrival: 16:05, 17:05, 18:05, 19:05
  - from: Oberau
    to: Munich
    by: RB 6
    departure: 16:15, 16:15, 17:15, 18:15
    arrival: 17:26, 18:26, 18:26, 19:26
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:15
  - location: Ettal
    geo: 47°34'07.6"N 11°05'36.0"E
    time: 9:05
route: https://www.alpenvereinaktiv.com/de/tour/aussichtsreiche-wanderung-von-ettal-nach-ettaler-sattel-ueber-die/306153876/?share=%7E3onspeua%244osscglp
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Drinks and Snacks
  - Warm clothes (forecast says 4°C)
sitemap_exclude: true
---
We've been in [Ettal](https://www.kloster-ettal.de/) before on the way to the [Linderhof](https://www.schlosslinderhof.de/).  
We won't stay there long this time and head to the next summit, the [Notkarspitze](https://en.wikipedia.org/wiki/Notkarspitze) (as alway, the english Wikipedia does not contain much more information then the location, there's more on the [german version](https://de.wikipedia.org/wiki/Notkarspitze)).

The ascend is quite rocky and steep (I was told), alpenvereinaktiv even rates the [route][route] to be hard, but I don't think it really is. Maybe it's not the best idea to have this be your veriy first tour or to join with sneakers.

The descent is rather easy, we walk along the edge and visit the Ziegelspitz and the Ochsenspitz before we return by public transport (or you walk back to your car).

I changed the registration to close on Saturday 7pm, if you cancel after, you steal someone's chance to join.

As people always ask for some WhatsApp Group, I now experiment with it, there's a [Notkarspitze](https://chat.whatsapp.com/DRZQrDJh7wV6gXasGWdqG4) group just for this hike.

[route]: https://www.alpenvereinaktiv.com/de/tour/aussichtsreiche-wanderung-von-ettal-nach-ettaler-sattel-ueber-die/306153876/?share=%7E3onspeua%244osscglp