---
title: "Sonnenberg 🔴 (medium, 1038hm, 7:00h)"
planned_date: 2024-10-06
facts:
  distance: 16 km
  duration: 7:00 h
  ascent: 1038 hm
  descent: 1142 hm
  highest: 1749 m
  lowest: 833 m
  difficulty: medium
  departure: 07:32
  return: 18:00
commute:
  - from: 'München'
    departure: 7:32
    by: RB 6
    to: Oberau
    direction: Garmisch-Patenkirchen
    arrival: 8:48
  - from: Oberau
    departure: 8:55
    by: Bus 9606
    to: Klostergasthof, Ettal
    direction: Oberammergau
    arrival: 9:05
  - from: Klostergasthof, Ettal
    departure: 9:39
    by: Bus 9622
    to: Linderhof Schloß
    arrival: 9:55
  - from: Linderhof Schloß
    to: August-Schuster Haus (Pürschlinghaus)
    departure: 10:00
  - from: August-Schuster Haus (Pürschlinghaus)
    to: Teufelstättkopf
    departure: 12:15
    comment: optional
  - to: August-Schuster Haus (Pürschlinghaus)
    from: Teufelstättkopf
    departure: 12:45
  - from: August-Schuster Haus (Pürschlinghaus)
    to: Sonnenberg
  - from: Sonnenberg
    to: Oberammergau
  - from: Oberammergau
    to: Oberau
    by: Bus 9606
    departure: 16:40, 17:40, 18:40
    arrival: 17:05, 18:05, 19:05
  - from: Oberau
    to: Munich Main Station
    by: RB 6
    departure: 17:15, 18:15, 19:15
    arrival: 18:26, 19:26, 20:26
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 7:15
  - location: Oberau
    geo: 47°33'32.6"N 11°08'17.9"E
    time: 08:50
  - location: Kloster Ettal
    geo: 47°34'07.6"N 11°05'36.0"E
    time: 09:05-9:39
  - location: Linderhof
    geo: 47°34'11.2"N 10°57'31.3"E
    time: 09:55
route: https://www.alpenvereinaktiv.com/de/tour/rabenkopf/302326645/?share=%7Ezzpxtmwm%244ossgotj
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Drinks and Snacks
  - Warm clothes (it is getting colder)
sitemap_exclude: true
---
Before the Pürschlhaus closes before the winter, we can take a short trip there.

Getting to the start takes a while using public transport, we even have 30 minutes available to shortly see the [Kloster Ettal][kloster] (just be back on time).  
When arriving, we can walk through the [Linderhof][linderhof] before starting the [tour][route] heading to the [Pürschlinghaus][haus].

This might be enough for you and you get something to drink/eat. You might also take a short detour to the Teufelstättkopf nearby and have the main break here.

When returning we walk along the Sonnenbergsteig (the main reason it's a medium tour, and I haven't yet been here) to the Sonnenberg and further to the train station on Oberammergau.

When you are participating and figure out you will not make it, please step back at least early afternoon on saturday, so that others know they can join.

[route]: https://www.alpenvereinaktiv.com/de/tour/lange-wanderung-auf-european-long-distance-path-e-4-part-germany/303163873/?share=%7Ezzvrpyhh%244ossk4ki
[haus]: https://august-schuster-haus.de/
[linderhof]: https://www.schlosslinderhof.de/
[kloster]: https://www.kloster-ettal.de/