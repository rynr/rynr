---
title: "Geierstein - Fockenstein"
sitemap_exclude: true
planned_date: 2024-04-07
facts:
  distance: 15.8
  duration: 6:30
  ascent: 1170
  descent: 1095
  highest: 1564
  lowest: 679
  difficulty: medium
  departure: 07:15
  return: 18:00
commute:
  - from: Munich Main Station
    departure: 07:30
    by: BRB RB56
    direction: Lenggries
    to: Lenggries
    arrival: 08:41
  - from: Lenggries
    departure: 08:45
    to: Geierstein
  - from: Geierstein
    to: Fockenstein
    departure: 11:30
  - from: Fockenstein
    to: Aueralm
    departure: 13:00
  - from: Aueralm
    to: Bad Wiessee
    departure: 14:45
  - from: Bad Wiessee, Söllbach
    by: Bus 357/359
    to: Gmund am Teegernsee
    departure: 15:32, 16:32, 17:02, 17:32
  - from: Gmund am Teegernsee
    by: BRB RB57
    to: Munich
meet:
  - location: Munich Main Station (Platforms 27-36)
    geo: 48°08'28.9"N 11°33'26.2"E
    time: 07:15
  - location: Lenggries Station
    geo: 47°40'49.0"N 11°34'26.0"E
    time: 08:45
bring:
  - Worn-in hiking-boots
  - Warm clothing, beanie, warm gloves
  - Rain-Protection
  - Drinks and Snacks (there's a hut, but you should have some on the tour)
  - Enough stamina for this hike
route: https://www.alpenvereinaktiv.com/de/tour/munich-international-hiking-fockenstein-06.05.2024/288998493/?share=%7Ezypufyti%244ossewfo
---
Are you ready for another hike?
The [Geierstein & Fockenstein][route] are a classic. It is part of the [Maximiliansweg](https://en.wikipedia.org/wiki/Maximiliansweg), which is part of the European long distance path [E4](https://en.wikipedia.org/wiki/E4_European_long_distance_path). As it's a known route we'll not be alone, so better do it at the beginning of the year.

We start in Lenggries, start going up at the [Kalvarienberg](https://de.wikipedia.org/wiki/Kalvarienberg) to our first summit, the [Geierstein](https://de.wikipedia.org/wiki/Geierstein), which is mostly going up. After a hort break we head to the next and main summit, the [Fockenstein](https://de.wikipedia.org/wiki/Fockenstein). Depending on the group we make a longer or shorter break and head to the [Aueralm](https://www.aueralm.de/), where we can have a drink or also something to eat, before we descend to [Bad Wiessee](https://www.tegernsee.com/bad-wiessee), where we take the Bus to Gmund and the train back to Munich.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html
[route]: https://www.alpenvereinaktiv.com/de/tour/munich-international-hiking-fockenstein-06.05.2024/288998493/?share=%7Ezypufyti%244ossewfo
