---
title: "Riederstein & Baumgartenscheid"
planned_date: 2024-05-01
facts:
  distance: 13.6
  duration: 5:00
  ascent: 778
  descent: 752
  highest: 1447
  lowest: 755
  difficulty: easy
  departure: 07:15
  return: 18:00
commute:
  - from: 'Munich Main Station'
    departure: 7:30
    by: BRB RB57
    to: Tegernsee
    arrival: 8:39
  - from: Tegernsee
    to: Riederstein
    departure: 8:40
  - from: Riederstein
    to: Baumgartenscheid
    departure: 10:00
  - from: Baumgartenscheid
    to: Schliersee
    departure: 11:30
  - from: Schliersee
    to: Munich
    departure: 14:30, 15:30, 16:01, 16:30
    by: BRB RB55
meet:
  - location: Munich Main Station
    lng: 48.1414729
    lat: 11.5566653
    time: 7:15
  - location: Tegernsee
    lng: 47.714248
    lat: 11.7571027
    time: 8:40
route: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-04-22/290867048/?share=%7Ezyejijxx%244ossvahb
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather, we will probably have snow even on the path somewhere.
  - Sun-Protection
  - Drinks and Snacks
  - Enough stamina for this hike
sitemap_exclude: true
---
I wasn't so sure about all the new snow last weekend, so I didn't setup a hike. So I was looking for a hike, which doesn't go that high, but is not that booring.

The [Tour][route] is this time between Tegernsee and Schliersee.  
We start in [Tegernsee](https://www.tegernsee.com/) with the first destination of the [Riederstein](https://de.wikipedia.org/wiki/Riederstein), from where we have a nice view on the Tegernsee. From here, the next summit is the [Baumgartenscheid](https://en.wikipedia.org/wiki/Baumgartenschneid). From here, you can see the Tegernsee, and also the Schliersee. You can already see the final destination as well.  
Now it's already the descent, which is further than what we did before. Unfortunately, there's no Hut on the path. We will enter the Train in Schliersee. I assume many people being around, so it could be crowded. But the hike is a rather quick one, so we could be lucky and still find some space.

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[route]: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-04-22/290867048/?share=%7Ezyejijxx%244ossvahb
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html