---
title: "Aiplspitz 🔴 (medium, 1161hm, 5:20h)"
planned_date: 2024-09-01
facts:
  distance: 11.8 km
  duration: 5:20 h
  ascent: 1161 hm
  descent: 789 hm
  highest: 1757 m
  lowest: 775 m
  difficulty: medium
  departure: 07:30
  return: 19:05
commute:
  - from: 'Munich Main Station'
    departure: 7:15
    by: BRB RB55
    to: Fischbachau
    direction: Bayrischzell
    arrival: 08:45
  - from: Fischbachau
    to: Aiplspitz
    departure: 08:50
  - from: Aiplspitz
    to: Jägerkamp
  - from: Jägerkamp
    to: Wilde Fräulein
  - from: Wilde Fräulein
    to: Schönfeldhütte
  - from: Schönfeldhütte
    to: Spitzingsattel
  - from: Spitzingsattel
    to: Schliersee
    by: Bus 362
    departure: 15:03, 15:33, 16:03, 16:32
    arrival: 15:21, 15:51, 16:21, 16:54
  - from: Schliersee
    to: München Hbf
    by: BRB BR55
    departure: 15:30, 16:01, 16:30, 17:01
    arrival: 16:32, 16:57 17:32, 17:57
meet:
  - location: Munich Main Station
    geo: 48°08'29.3"N 11°33'24.0"E
    time: 07:15
  - location: Bahnhof Fischbachau
    geo: 47°42'21.4"N 11°55'59.0"E
    time: 08:45
route: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-08-28/300766332/?share=%7Ezzfbihvi%244ossttu9
bring:
  - Worn-in hiking-boots
  - Clothing according to the weather
  - Sun-Protection
  - Drinks and Snacks
  - Money for the hut (optional)
  - Swimsuit (optional)
sitemap_exclude: true
---
Another weekend with promising weather (at least it stays dry so far).

The ascent to [Aiplspitz](https://en.wikipedia.org/wiki/Aiplspitz) (there's more in the [german Wikipedia article](https://de.wikipedia.org/wiki/Aiplspitz)) is a fun one, there's some height meters to cross, and at the end it get's a bit more fun. That's why I think it's not a beginner tour, but it's also not a hard one.  
From here, heading to Jägerkamp is a classic, but I've yet not been at the "[Wilde Fräulein](https://de.wikipedia.org/wiki/Wilde_Fr%C3%A4ulein)", so we'll see how this turns out.  
Of course there's a [hut](https://www.alpenverein-muenchen-oberland.de/huetten/alpenvereinshuetten/schoenfeldhuette), where we can have something to drink, maybe something to eat.  
We don't need to descent back to the beginning, only to the Spitzbergsattel.

See the full track at [alpenverenaktiv][route].

From here there are multiple ways to return to munich. The easiest is to use the Bus 362, change in Schliersee and get back to munich. We everybody usualy has a Deutschland-Ticket, you can also decide to exit the bus and make a stop at Schliersee for a short swim, or you can also descent further from Spitzingsattel. This is all on you (my current preference is to take a break at the Spitzingsee, but we'll see how the weather will be).

If you join by train, I assume you to have a [Deutschland-Ticket][dticket]. If not, please check who else needs a ticket and align to get one on time.

[route]: https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-08-28/300766332/?share=%7Ezzfbihvi%244ossttu9
[dticket]: https://www.mvg.de/tickets-tarife/abonnement/deutschlandticket.html

