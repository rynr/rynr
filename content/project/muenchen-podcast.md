+++
date = '2025-01-01T00:00:00+01:00'
title = 'München Podcast'
description = 'Podcast about news and interesting stuff in Munich'
tags = ['podcast', 'munich', 'news']
started = 'January 2025'
projectUrl = 'https://www.bekleben-verboten.com/'
+++
> Let's start a podcast and get rich.

That's how it started after I had the challenge to visit a beergarden every month in 2024.

We're still setting up everything, the current domain for the project is [bekleben-verboten.com](https://www.bekleben-verboten.com/), the plan is to record one episode every month while visiting a beergarden.
