+++
date = '2016-01-01T00:00:00+01:00'
title = 'Javacasts'
description = 'Screencast Tutorials for Java'
tags = ['tutorial', 'video', 'java']
started = 'January 2016'
ended = 'February 2017'
projectUrl = 'https://javacasts.net/'
+++
My goal was, to create video casts on Java themes, inspired by [railscasts.com](http://railscasts.com/).

In the end, I didn't do more videos, as there was quite some effort involved in creating them. I mostly didn't like the effort to the vocal part, and especially synchronization between audio and video.  
I now use different video software, so it might work better now, but I don't have that much drive to make new videos.

{{< youtube-playlist "PLXID3e3on0qt9YsUdLLnzNmHmQvYLEjJG" >}}
