+++
date = '2023-12-16T18:30:30+01:00'
title = 'UnderCover'
description = 'My current main Rock-Band. Happy to see you on a gig.'
tags = ['music', 'covers', 'video', 'recording']
started = '2022'
projectUrl = 'https://www.undercovermunich.de/'
+++
My current main Band. It's most of the people I played with in my
previous band Fully Recovered.  
We are playing rock covers from the 70s til more recent songs.

We try to also make videos of our gigs, or just a song we like to
play, so we also have a growing list of videos on Youtube.

<!--more-->

Our setlists are available at [setlist.fm](https://www.setlist.fm/setlists/undercover-munich-43e7eb7f.html), I also keep them up to date at [musikbrainz](https://musicbrainz.org/artist/ef669bb2-00cb-4d31-8481-e144d0b4a871/events).

Recording Videos
----------------

{{< youtube-playlist "PLxlZUEGTN-fOFV-8vZi0Toi9obZXXqdnE" >}}

Live Videos
-----------

{{< youtube-playlist "PLxlZUEGTN-fPgsf3Hf5zEAqka5utEcWn3" >}}
