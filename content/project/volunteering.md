+++
date = '2024-09-23T10:37:30+01:00'
title = 'Volunteering'
description = 'I like volunteering'
tags = ['volunteering', 'events']
started = '2022'
+++
I started volunteering in 2021 for the München Marathon. Since then I did some more volunteer jobs.

<!--more-->

- [Bundestagswahl 2025](https://stadt.muenchen.de/wahlhelfer) 2025  
  Wahlhelfer (noch nicht bestätigt)
- [München Marathon](https://www.generalimuenchenmarathon.de/) 2024  
  Fanshop & Baggage Drop-off
- [Bladenight München](https://bladenight-muenchen.de/) 2024  
  Bladeguard, taking care of the participants
- [UEFA Euro 2024](https://uefa.com/euro2024/) 2024  
  Fan-Welcome close to the Fan-Zone
- [Bladenight München](https://bladenight-muenchen.de/) 2023  
  Bladeguard, taking care of the participants
- [Landtagswahl Bayern](https://www.landtagswahl2023.bayern.de/) 2023  
  Wahlhelfer
- [Xletix](https://www.xletix.com/) 2023  
  Refreshment station (on track & finish line)
- [München Marathon](https://www.generalimuenchenmarathon.de/) 2022  
  Driving participants and fetching stuff from Baumarkt (like buckets, cable ties, …).  
  Diving of all equipment to supply stations and back.

