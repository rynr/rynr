+++
date = '2022-06-01T01:00:00+01:00'
title = 'Fully-Recovered'
description = 'Pop&Rock-Band, my first gigs in known Pubs in munich.'
tags = ['music', 'covers', 'video']
started = 'September 2019'
ended = '2022'
+++
A cover band I played with. I had my first gig in the [Kilians][kilians]
in munich. After a longer break it was nice playing again regularely.

{{< youtube "G8XXvjl52PE" >}}

[kilians]: https://kiliansirishpub.de/live-music/
