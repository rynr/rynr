+++
date = '2023-12-19T17:43:41+01:00'
title = 'Lighting Control'
description = 'Lightshow Control Software to control the lights handsfree.'
tags = ['software', 'java']
started = 'August 2023'
projectUrl = 'https://gitlab.com/lightingcontrol/'
+++
The goal is simply explained. I want to control a loght show during concerts where I already play an instrument. So it's ideally fully automated or at least hands-free controlled.  
I already had a version (partially) working on a show, now it needs to work better. I'm working on a Frontend I can use to setup the show and later to run it.

A Preview can be seen at <https://lightingcontrol.gitlab.io/>.

More to come (hopefully soon).
