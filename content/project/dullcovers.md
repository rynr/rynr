+++
date = '2023-03-01T00:00:00+01:00'
title = 'Dullcovers'
description = 'Home-Recording during the Corona Epidemie & Meetup for video recording.'
tags = ['music', 'recording', 'video', 'covers', 'meetup']
started = 'December 2020'
ended = 'February 2023'
projectUrl = 'https://dullcovers.eu/'
+++
When the Corona Pandemy caused the need to stay at home. Making music
was hard at that time. So I was searching for other who want to record
some music.  
We decided on a song, prepared some computer generated tracks, so
everybody could practice and record his stems.
<!--more-->
Once everything was recorded, it was mixed and put online.
We recorded 22 songs in this time.
{{< soundcloud-playlist "1223001253" >}}

After the corona restrictions were lifted again, we just finished the
open songs and stopped the recording, but I started some life-recording
sessions. Instead of recording everbody on it's own, we were meeting
in a rehearsal room and recorded one song per month, this time, we also
recorded a video.  
We recorded 8 Songs/Videos.

{{< youtube-playlist "PLYA18PjJlxqkA14BwJDOrhf3xHz3HYyhb" >}}

After the project requested some effort (preparation, mixing, video
production), I was happy to end it after 9 months. Thanks to everybody
who participated in the project. I especially way happy to be able to
record music with my brother, though we don't have that many things we
do together.  
I think we made some good recordings.
