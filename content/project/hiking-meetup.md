+++
date = '2023-01-01T00:00:00+01:00'
title = 'Hiking Meetup'
description = 'Organizing Hikes to the alps in a Meetup Group.'
tags = ['hiking','meetup']
started = 'December 2022'
projectUrl = 'https://www.meetup.com/Munich-International-Hiking/'
+++
Meetup gives a platform to schedule events with others to join. I did a
lot of hiking with Stefan, who was the owner of another hiking group.
After him finding a good partner, he didn't find the time any more, so
we just meet once or twice the year for a nice hike.

I had quite a number of hikes in 2024, like [Pendling][hike-pendling], [Kramerspitze][hike-kramerspitz], [Schildenstein][hike-schildenstein], [Brecherspitze][hike-brecherspitz], [Leonhardstein & Seekarkreuz][hike-leonhardstein], [Riederstein & Baumgartenscheid][hike-riederstein], [Hoher Fricken][hike-fricken], [Geierstein][hike-geierstein] and [Seebergkopf][hike-seebergkopf].

[hike-pendling]: https://www.meetup.com/munich-international-hiking/events/302310935/
[hike-kramerspitz]: https://www.meetup.com/munich-international-hiking/events/301620044/
[hike-schildenstein]: https://www.meetup.com/munich-international-hiking/events/301198390/
[hike-brecherspitz]: https://www.meetup.com/munich-international-hiking/events/301059690/
[hike-leonhardstein]: https://www.meetup.com/munich-international-hiking/events/300918718/
[hike-riederstein]: https://www.meetup.com/munich-international-hiking/events/300713342/
[hike-fricken]: https://www.meetup.com/munich-international-hiking/events/300350870/
[hike-geierstein]: https://www.meetup.com/munich-international-hiking/events/299983964/
[hike-seebergkopf]: https://www.meetup.com/munich-international-hiking/events/299792350/