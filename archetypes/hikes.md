---
title: ""
planned_date: 2024-01-07
commute:
  - from: 'Munich Main Station'
    departudr: 07:00
    by: Train
    direction: Lenggries
    to: Lenggries
    arrival: 08:30
meet:
  - location: Munich Main Station
    lng: 48.1414729
    lat: 11.5566653
    time: 06:45
route: http://www.outdooractive.com
equipment:
  - name: Snowshoes
    required: true
sitemap_exclude: true
draft: true
---
